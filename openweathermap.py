import urllib
import json
import datetime
import calendar
import time

OPENWEATHERMAP_URL = 'http://api.openweathermap.org/data/2.5/weather?q=Frauenaurach,de&units=metric'

def get_data():
	response = urllib.urlopen(OPENWEATHERMAP_URL)
	data =  json.loads(response.read())
	print data
	temp = data['main']['temp']
	sunrise = data['sys']['sunrise']
	sunset = data['sys']['sunset']
	return [temp, sunrise, sunset]
		
def main():
	data = get_data()
	print data[0]
	print time.strftime("%H%M", time.localtime(data[1]))
	print time.strftime("%H%M", time.localtime(data[2]))

if __name__ == '__main__':
	main()

